
%global oracle_client_major_version 21

Name:       ocilib
Version:    4.7.3
Release:    1%{?dist}
Summary:    ocilib provides access to Oracle databases for C programs

License:    Apache 2.0
Group:      Applications/Databases
URL:        https://github.com/vrogier/ocilib
Source0:    %{url}/releases/download/v%{version}/%{name}-%{version}-gnu.tar.gz

%if 0%{?rhel}
BuildRequires:  gcc-toolset-10
%else
BuildRequires:  gcc-c++
%endif
BuildRequires:  oracle-instantclient-devel >= %{oracle_client_major_version}.0.0.0.0
Requires:       oracle-instantclient-basic >= %{oracle_client_major_version}.0.0.0.0

%description
OCILIB is an open source and cross platform Oracle Driver that
delivers efficient access to Oracle databases. It offers a full
featured, easy and productive API. Written in ISO C on top of OCI,
OCILIB runs on all Oracle platforms.


%package devel
Summary: Development files for ocilib
Requires: %{name} = %{version}
Requires: pkgconfig

%description devel
OCILIB is an open source and cross platform Oracle Driver that
delivers efficient access to Oracle databases. It offers a full
featured, easy and productive API. This package contains files
required for developing applications and/or libraries against
ocilib.


%prep
%setup -q -n %{name}-%{version}


%build
%if 0%{?rhel}
scl enable gcc-toolset-10 - << \SCLEOF
set -e
%endif
%configure \
    --with-oracle-headers-path=/usr/include/oracle/%{oracle_client_major_version}/client64 \
    --with-oracle-lib-path=/usr/lib/oracle/%{oracle_client_major_version}/client64/lib \
    --with-oracle-import=linkage

%make_build
%if 0%{?rhel}
SCLEOF
%endif


%install
%make_install


%files
%{_libdir}/*.so*


%files devel
%{_includedir}
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.a
%{_libdir}/*.la
%doc %{_datadir}/doc/%{name}


%changelog
* Sat Jun 26 2021 Levent Demirörs <levent.demiroers@profidata.com> - 4.7.3-1
- Upgrade to version 4.7.3
- Upgrade to GCC 10
